-- LUALOCALS < ---------------------------------------------------------
local minetest, nodecore
    = minetest, nodecore
-- LUALOCALS > ---------------------------------------------------------

local modname = minetest.get_current_modname()

local orespots = "nc_cats_base.png^[multiply:#ff8000"
.. "^(nc_cats_spots_x2.png^[multiply:#804000)"

local strata = {modname .. ":ore"}
for i = 1, nodecore.hard_stone_strata do
	strata[i + 1] = modname .. ":ore_" .. i
end
for i = 0, nodecore.hard_stone_strata do
	minetest.register_node(strata[i + 1], {
			description = "Cat Ore",
			tiles = {
				nodecore.hard_stone_tile(i) .. "^(" .. orespots
				.. "^[mask:nc_lode_mask_ore.png^[opacity:80)"
			},
			is_ground_content = true,
			groups = {
				[modname .. "_ore"] = i,
				hard_stone = i > 0 and i or nil,
				stone = i + 1,
				cracky = i + 2,
			},
			sounds = nodecore.sounds("nc_terrain_stony"),
			drop_in_place = modname .. ((i > 1)
				and (":ore_" .. (i - 1)) or (i > 0) and ":ore" or ":cobble"),
			strata = strata,
			silktouch = false,
			mapcolor = {r = 69, g = 58, b = 53},
		})
end

minetest.register_node(modname .. ":cobble", {
		description = "Cat Cobble",
		tiles = {
			"nc_terrain_gravel.png^(" .. orespots
			.. "^[mask:nc_lode_mask_ore.png^[opacity:128)^nc_terrain_cobble.png"
		},
		is_ground_content = true,
		groups = {
			cracky = 2,
			cobbley = 1
		},
		alternate_loose = {
			repack_level = 2,
			groups = {
				cracky = 0,
				crumbly = 2,
				falling_repose = 3
			},
			sounds = nodecore.sounds("nc_terrain_chompy")
		},
		sounds = nodecore.sounds("nc_terrain_stony"),
		mapcolor = {r = 62, g = 52, b = 59},
	})

minetest.register_node(modname .. ":egg", {
		description = "Cat Cocoon",
		tiles = {
			orespots .. "^nc_terrain_cobble.png"
		},
		groups = {
			cracky = 1,
			cobbley = 1
		},
		alternate_loose = {
			repack_level = 2,
			groups = {
				cracky = 0,
				crumbly = 2,
				falling_repose = 3
			},
			sounds = nodecore.sounds("nc_terrain_chompy")
		},
		stack_max = 1,
		on_place = minetest.rotate_and_place,
		sounds = nodecore.sounds("nc_terrain_stony"),
		mapcolor = {r = 81, g = 60, b = 54},
	})

minetest.register_craftitem(modname .. ":prill", {
		description = "Cat Prill",
		inventory_image = orespots .. "^[mask:nc_lode_mask_prill.png",
		sounds = nodecore.sounds(modname .. "_mew", nil, 1.5)
	})

minetest.register_node(modname .. ":packed", {
		description = "Cat Fluff",
		tiles = {
			orespots
		},
		groups = {
			crumbly = 1,
			[modname .. "_packed_fluff"] = 1
		},
		alternate_loose = {
			groups = {
				falling_repose = 1
			},
		},
		sounds = nodecore.sounds(modname .. "_mew", nil, 1.5),
		mapcolor = {r = 192, g = 96, b = 0},
	})
