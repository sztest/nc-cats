-- LUALOCALS < ---------------------------------------------------------
local ipairs, minetest, nodecore, pairs
    = ipairs, minetest, nodecore, pairs
-- LUALOCALS > ---------------------------------------------------------

local modname = minetest.get_current_modname()
local myapi = _G[modname]

local desc2name = myapi.desc2name
local getcatid = myapi.getcatid

local function reg(legacyid, desc, ...)
	myapi.register_cat_spots(desc, ...)
	local id = desc2name(desc)
	for _, n in ipairs({":cat_", ":longcat_front_", ":longcat_mid_", ":longcat_back_"}) do
		minetest.register_alias(modname .. n .. legacyid, modname .. n .. id)
	end
end
reg(1, "Light Gray", "#c0c0c0", "#404040")
reg(2, "Medium Gray", "#404040", "#202020")
reg(3, "Dark Gray", "#202020", "#101010")
reg(4, "Dark Orange", "#ff8000", "#804000")
reg(5, "Light Orange", "#e0c040", "#ff8000")
reg(6, "Dark Brown", "#804000", "#402000")
reg(7, "Light Brown", "#c0a080", "#806020")

local function getflowerid(node)
	local def = node and node.name and minetest.registered_nodes[node.name]
	return def and def.nc_flower_color
end

local function regflower(desc, flower_color_id, base, spot)
	local id = desc2name(desc)
	myapi.register_cat_spots(desc, base, spot, {
			[modname .. "_spawn_probability"] = function(pos)
				local prob = myapi.default_spawn_probability_function(id)(pos)
				for _, np in ipairs(nodecore.find_nodes_around(pos,
						{"group:flower_living"}, 2)) do
					if getflowerid(minetest.get_node(np)) == flower_color_id then
						prob = prob * 1.1
					end
				end
				if prob > 3 then prob = 3 end
				return prob - 1
			end,
			groups = {[modname .. "_floral"] = 1}
		})
end
regflower("Pink", 1, "#d23379", "#9a2357")
regflower("Red", 2, "#d80000", "#9e0000")
regflower("Azure", 6, "#39849b", "#275f71")
regflower("Blue", 7, "#2424fe", "#1717bb")
regflower("Violet", 8, "#5900b2", "#3f0082")

do
	local side = "nc_cats_rainbow.png^(nc_cats_rainbow.png^[multiply:#808080^[mask:nc_cats_spots.png)"
	local top = "nc_cats_base.png^[multiply:#a840ff^(nc_cats_spots.png^[multiply:#7a2cbc)"

	-- N.B. do not defer expansion, we only want to require cats defined here, not expansion ones.
	local allcats = nodecore.group_expand({"group:" .. modname .. "_shortcat"}, false)
	local total = 0
	for _ in pairs(allcats) do total = total + 1 end

	myapi.register_cat("Rainbow", {top, top, side, side, side, side}, {
			[modname .. "_spawn_probability"] = function(pos)
				local qty = 0
				local seen = {}
				for _, np in ipairs(nodecore.find_nodes_around(pos,
						{"group:" .. modname .. "_cat"}, 2)) do
					local nid = getcatid(minetest.get_node(np))
					if not seen[nid] then
						seen[nid] = true
						qty = qty + 1
					end
				end
				return qty >= total and 1 or 0
			end,
			groups = {[modname .. "_rainbow"] = 1},
			mapcolor = {r = 147, g = 55, b = 225}
		})
end
