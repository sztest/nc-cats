-- LUALOCALS < ---------------------------------------------------------
local ipairs, math, minetest, nodecore, pairs, string, tonumber, type,
      vector
    = ipairs, math, minetest, nodecore, pairs, string, tonumber, type,
      vector
local math_random, string_gsub, string_lower, string_sub
    = math.random, string.gsub, string.lower, string.sub
-- LUALOCALS > ---------------------------------------------------------

local modname = minetest.get_current_modname()
local myapi = _G[modname]

local cat_type_key = modname .. "_type"
local function getcatid(node)
	local def = node and node.name and minetest.registered_nodes[node.name]
	return def and def[cat_type_key]
end
myapi.getcatid = getcatid

local function faceallow(pos, param2)
	local face = nodecore.facedirs[param2]
	local fp = vector.add(pos, face.f)
	local node = minetest.get_node(fp)
	local def = minetest.registered_nodes[node.name] or {}
	return def.sunlight_propagates or def.air_equivalent
end
local function checkfacedir(pos)
	local node = minetest.get_node(pos)
	if faceallow(pos, node.param2) then return end
	local t = {}
	for i = 0, 23 do
		if faceallow(pos, i) then t[#t + 1] = i end
	end
	if #t < 1 then return end
	node.param2 = t[math_random(1, #t)]
	minetest.swap_node(pos, node)
	return node
end
myapi.checkfacedir = checkfacedir

local function longcatmatch(nodea, nodeb)
	return getcatid(nodea) == getcatid(nodeb)
	and vector.equals(
		nodecore.facedirs[nodea.param2].f,
		nodecore.facedirs[nodeb.param2].f)
end
myapi.longcatmatch = longcatmatch
local function findtail(pos, node, ishead)
	local fd = nodecore.facedirs[node.param2]
	local back = vector.add(pos, fd.k)
	local bnode = minetest.get_node(back)
	local istail = not longcatmatch(node, bnode)

	local catname = not istail and findtail(back, bnode, false)

	local part = ishead and istail and ":cat_"
	or ishead and ":longcat_front_"
	or istail and ":longcat_back_"
	or ":longcat_mid_"
	node.name = modname .. part .. getcatid(node)
	if not istail then node.param2 = bnode.param2 end

	local saved = minetest.get_meta(pos):to_table()
	saved.fields.realname = saved.fields.realname or saved.fields.description
	saved.fields.description = istail and saved.fields.realname or catname or ""
	nodecore.set_node_check(pos, node)
	minetest.get_meta(pos):from_table(saved)
	return saved.fields.description
end
local function findhead(pos, node)
	local fd = nodecore.facedirs[node.param2]
	local fore = vector.add(pos, fd.f)
	local fnode = minetest.get_node(fore)
	if longcatmatch(node, fnode) then
		return findhead(fore, fnode)
	end
	findtail(pos, node, true)
end
local function checksnake(pos, node)
	node = node or minetest.get_node(pos)
	if not getcatid(node) then return end
	return findhead(pos, node)
end

function myapi.default_spawn_probability_function(id)
	return function(pos)
		local prob = 1
		for _, np in ipairs(nodecore.find_nodes_around(pos,
				{"group:" .. modname .. "_cat"}, 2)) do
			if getcatid(minetest.get_node(np)) == id then
				prob = prob * 1.2
			end
		end
		if prob > 3 then prob = 3 end
		return prob
	end
end

local function fuzztiles(tiles)
	for i = 1, #tiles do
		tiles[i] = tiles[i] .. "^nc_cats_fuzz.png"
	end
	return tiles
end

local default_overlays = {
	ears = "^nc_cats_ears.png",
	paws = "^nc_cats_paws_front.png^nc_cats_paws_back.png",
	tail = "^nc_cats_tail.png",
	face = "^nc_cats_face.png",
	paws_front = "^nc_cats_paws_front.png",
	paws_middle = "^nc_cats_paws_mid.png",
	paws_back = "^nc_cats_paws_back.png",
}

local function desc2name(s) return string_gsub(string_lower(s), "%W+", "_") end
myapi.desc2name = desc2name

function myapi.register_cat(desc, basetiles, basedef, overlays)
	local id = basedef and basedef[cat_type_key] or desc2name(desc)

	basetiles = basetiles or basedef.tiles
	basetiles[2] = basetiles[2] or (basetiles[1] .. "^[transformR90")
	basetiles[3] = basetiles[3] or (basetiles[1] .. "^[transformFX")
	basetiles[4] = basetiles[4] or basetiles[1]
	basetiles[5] = basetiles[5] or basetiles[3]
	basetiles[6] = basetiles[6] or basetiles[1]

	overlays = nodecore.underride(overlays or {}, default_overlays)

	basedef = nodecore.underride(basedef or {}, {
			description = desc .. " Cat",
			tiles = fuzztiles({
					basetiles[1] .. overlays.ears,
					basetiles[2] .. overlays.paws,
					basetiles[3],
					basetiles[4],
					basetiles[5] .. overlays.tail,
					basetiles[6] .. overlays.face,
				}),
			[cat_type_key] = id,
			[modname .. "_spawn_probability"] = myapi.default_spawn_probability_function(id),
			groups = {
				snappy = 1,
				falling_node = 1,
				[modname .. "_cat"] = 1
			},
			stack_max = 1,
			paramtype2 = "facedir",
			on_place = function(stack, placer, pointed, inf, orient, ...)
				orient = orient or {}
				orient.invert_wall = pointed.above.y == pointed.under.y
				return minetest.rotate_and_place(stack, placer, pointed, inf, orient, ...)
			end,
			sounds = nodecore.sounds(modname .. "_mew"),
			drop = modname .. ":cat_" .. id,
			preserve_metadata = function(_, _, oldmeta, drops)
				if oldmeta.realname then
					oldmeta.description = oldmeta.realname
					oldmeta.realname = nil
				end
				drops[1]:get_meta():from_table({fields = oldmeta})
			end,
			after_place_node = function(pos, _, itemstack)
				local node = checkfacedir(pos)
				local meta = minetest.get_meta(pos)
				meta:from_table(itemstack:get_meta():to_table())
				myapi.setname(meta)
				checksnake(pos, node)
			end,
			after_destruct = function(pos)
				for _, dir in ipairs(nodecore.dirs()) do
					local p = vector.add(pos, dir)
					checksnake(p)
				end
			end
		})

	minetest.register_node(":" .. modname .. ":cat_" .. id, nodecore.underride({
				groups = {
					[modname .. "_face"] = 1,
					[modname .. "_shortcat"] = 1
				},
			}, basedef))

	minetest.register_node(":" .. modname .. ":longcat_front_" .. id,
		nodecore.underride({
				description = desc .. " LongCat Head",
				[modname .. "_spawn_probability"] = false,
				tiles = fuzztiles({
						basetiles[1] .. overlays.ears,
						basetiles[2] .. overlays.paws_front,
						basetiles[3],
						basetiles[4],
						basetiles[5],
						basetiles[6] .. overlays.face,
					}),
				groups = {
					falling_node = 0,
					[modname .. "_face"] = 1,
					[modname .. "_longcat"] = 1
				},
				drop = modname .. ":cat_" .. id
			}, basedef))
	minetest.register_node(":" .. modname .. ":longcat_mid_" .. id,
		nodecore.underride({
				description = desc .. " LongCat Midsection",
				[modname .. "_spawn_probability"] = false,
				tiles = fuzztiles({
						basetiles[1],
						basetiles[2] .. overlays.paws_middle,
						basetiles[3],
						basetiles[4],
						basetiles[5],
						basetiles[6],
					}),
				groups = {
					falling_node = 0,
					[modname .. "_longcat"] = 1
				},
				drop = modname .. ":cat_" .. id
			}, basedef))
	minetest.register_node(":" .. modname .. ":longcat_back_" .. id,
		nodecore.underride({
				description = desc .. " LongCat Tail",
				[modname .. "_spawn_probability"] = false,
				tiles = fuzztiles({
						basetiles[1],
						basetiles[2] .. overlays.paws_back,
						basetiles[3],
						basetiles[4],
						basetiles[5] .. overlays.tail,
						basetiles[6],
					}),
				groups = {
					falling_node = 0,
					[modname .. "_longcat"] = 1
				},
				drop = modname .. ":cat_" .. id
			}, basedef))
end

function myapi.register_cat_spots(id, base, spot, basedef, ...)
	local txr = "nc_cats_base.png^[multiply:" .. base
	if base ~= spot then
		txr = txr .. "^(nc_cats_spots.png^[multiply:" .. spot .. ")"
	end
	basedef = nodecore.underride(basedef or {}, {
			mapcolor = {
				r = (tonumber(string_sub(base, 2, 3), 16)
					+ tonumber(string_sub(spot, 2, 3), 16)) / 2,
				g = (tonumber(string_sub(base, 4, 5), 16)
					+ tonumber(string_sub(spot, 4, 5), 16)) / 2,
				b = (tonumber(string_sub(base, 6, 7), 16)
					+ tonumber(string_sub(spot, 6, 7), 16)) / 2,
			}
		})
	return myapi.register_cat(id, {txr}, basedef, ...)
end

function myapi.makecat(pos, param2, creator)
	local all = {}
	for k, v in pairs(minetest.registered_nodes) do
		if v[modname .. "_spawn_probability"] then
			all[k] = v[modname .. "_spawn_probability"](pos)
		end
	end
	local _, nodename = nodecore.pickrand(all, function(v) return v end)
	nodecore.set_loud(pos, {
			name = nodename,
			param2 = type(param2) == "number" and param2
			or math_random(0, 3)
		})

	local meta = minetest.get_meta(pos)
	meta:set_float("birthtime", nodecore.gametime)
	meta:set_string("birthpos", minetest.pos_to_string(pos))
	if creator and creator.is_player and creator:is_player() then
		meta:set_string("creator", creator:get_player_name())
	end
	myapi.setname(meta)

	checkfacedir(pos)

	nodecore.fallcheck(pos)
end
